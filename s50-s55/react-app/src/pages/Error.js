import { Link } from "react-router-dom";

export default function Error() {
  return (
    <>
      <h1 className = "py-3">Page Not Found</h1>
      
      <p>
        Go back to the{" "}
        <Link to= '/'>
          homepage
        </Link>
        
      </p>
    </>
  );
}
