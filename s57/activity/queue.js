let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array

    return collection

};

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    collection[collection.length] = element;

    return collection
   
};

function dequeue() {
    // In here you are going to remove the first element in the array
    let afterCollection = [];

    for (let i = 1; i < collection.length; i++) {
        afterCollection[i - 1] = collection[i];
    }

    collection = afterCollection;

    return collection;

}

function front() {
    // you will get the first element

    return collection[0]
};


function size() {
  // Number of elements
  let size = 0;
  while (collection[size] !== undefined) {
    size++;
  }
  return size;
}


function isEmpty() {
    //it will check whether the function is empty or not

    let size = 0;
  while (collection[size] !== undefined) {
    size++;
  }
  return size === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};